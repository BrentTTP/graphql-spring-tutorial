package company.tothepoint.graphqlspringtutorial.model;

import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Document(collection = "games")
public class Game {

    private ObjectId id;

    private String title;
    private String genre;
    private Integer ageRestriction;
}
