package company.tothepoint.graphqlspringtutorial.resolver;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import company.tothepoint.graphqlspringtutorial.exception.GameNotFoundException;
import company.tothepoint.graphqlspringtutorial.exception.PlayerNotFoundException;
import company.tothepoint.graphqlspringtutorial.model.Game;
import company.tothepoint.graphqlspringtutorial.model.Player;
import company.tothepoint.graphqlspringtutorial.repository.GameRepository;
import company.tothepoint.graphqlspringtutorial.repository.PlayerRepository;
import lombok.AllArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

import javax.swing.text.html.Option;
import java.util.Optional;

import static java.util.Optional.ofNullable;

@Component
@AllArgsConstructor
public class MutationResolver implements GraphQLMutationResolver {

    private PlayerRepository playerRepository;
    private GameRepository gameRepository;

    public Game createGame(String title, String genre, Integer ageRestriction) {
        Game newGame = Game.builder().title(title).genre(genre).ageRestriction(ageRestriction).build();
        gameRepository.save(newGame);
        return newGame;
    }

    public Player createPlayer(String name, Integer age) {
        Player newPlayer = Player.builder().name(name).age(age).build();
        playerRepository.save(newPlayer);
        return newPlayer;
    }

    public String removeGame(String gameId) {
        gameRepository.deleteById(new ObjectId(gameId));
        return gameId;
    }

    public String removePlayer(String playerId) {
        playerRepository.deleteById(new ObjectId(playerId));
        return playerId;
    }

    public Game updateGame(String id, String title, String genre, Integer ageRestriction) {
        Optional<Game> fromDb = gameRepository.findById(new ObjectId(id));
        if (fromDb.isPresent()) {
            Game updatedGame = fromDb.get().toBuilder()
                    .title(title)
                    .genre(genre)
                    .ageRestriction(ageRestriction)
                    .build();
            gameRepository.save(updatedGame);
            return updatedGame;
        }
        throw new GameNotFoundException("The game you are trying to update does not exist", id);
    }

    public Player updatePlayer(String id, String name, Integer age) {
        Optional<Player> fromDb = playerRepository.findById(new ObjectId(id));
        if (fromDb.isPresent()) {
            Player updatedPlayer = fromDb.get().toBuilder()
                    .name(name)
                    .age(age)
                    .build();
            playerRepository.save(updatedPlayer);
            return updatedPlayer;
        }
        throw new PlayerNotFoundException("The player you are trying to update does not exist", id);
    }
}
