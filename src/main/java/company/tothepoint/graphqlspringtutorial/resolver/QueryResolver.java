package company.tothepoint.graphqlspringtutorial.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import company.tothepoint.graphqlspringtutorial.exception.GameNotFoundException;
import company.tothepoint.graphqlspringtutorial.exception.PlayerNotFoundException;
import company.tothepoint.graphqlspringtutorial.model.Game;
import company.tothepoint.graphqlspringtutorial.model.Player;
import company.tothepoint.graphqlspringtutorial.repository.GameRepository;
import company.tothepoint.graphqlspringtutorial.repository.PlayerRepository;
import lombok.AllArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@AllArgsConstructor
public class QueryResolver implements GraphQLQueryResolver {

    private PlayerRepository playerRepository;
    private GameRepository gameRepository;

    public List<Player> getPlayers(String name) {
        if (name == null) return playerRepository.findAll();
        else              return playerRepository.findAllByName(name);
    }

    public Player getPlayer(String id) {
        Optional<Player> oPlayer = playerRepository.findById(new ObjectId(id));
        if (oPlayer.isPresent())
            return oPlayer.get();
        throw new PlayerNotFoundException("The player you asked for does not exist", id);
    }

    public List<Game> getGames() {
        return (ArrayList<Game>) gameRepository.findAll();
    }

    public Game getGame(String id) {
        Optional<Game> oGame = gameRepository.findById(new ObjectId(id));
        if (oGame.isPresent())
            return oGame.get();
        throw new GameNotFoundException("The game you asked for does not exist", id);
    }
}
