package company.tothepoint.graphqlspringtutorial.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import company.tothepoint.graphqlspringtutorial.model.Game;
import company.tothepoint.graphqlspringtutorial.model.Player;
import company.tothepoint.graphqlspringtutorial.repository.GameRepository;
import company.tothepoint.graphqlspringtutorial.repository.PlayerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class PlayerResolver implements GraphQLResolver<Player> {

    private GameRepository gameRepository;
    private PlayerRepository playerRepository;

    public List<Game> getGames(Player player) {
        return gameRepository.findByIdIn(player.getGamesIds());
    }

    public List<Player> getFriends(Player player) {
        return playerRepository.findByIdIn(player.getFriendsIds());
    }
}
