package company.tothepoint.graphqlspringtutorial.repository;

import company.tothepoint.graphqlspringtutorial.model.Game;
import org.bson.types.ObjectId;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface GameRepository extends PagingAndSortingRepository<Game, ObjectId> {

    List<Game> findByIdIn(List<String> ids);
}
