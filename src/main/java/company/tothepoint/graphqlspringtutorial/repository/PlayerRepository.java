package company.tothepoint.graphqlspringtutorial.repository;

import company.tothepoint.graphqlspringtutorial.model.Player;
import org.bson.types.ObjectId;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PlayerRepository extends PagingAndSortingRepository<Player, ObjectId> {

    List<Player> findAll();
    List<Player> findByIdIn(List<String> ids);
    List<Player> findAllByName(String name);
}
