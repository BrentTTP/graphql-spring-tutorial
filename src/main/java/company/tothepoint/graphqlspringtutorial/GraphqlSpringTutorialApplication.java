package company.tothepoint.graphqlspringtutorial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraphqlSpringTutorialApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraphqlSpringTutorialApplication.class, args);
	}
}
